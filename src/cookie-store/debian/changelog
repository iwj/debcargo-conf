rust-cookie-store (0.21.1-1) unstable; urgency=medium

  * Team upload.
  * Package cookie_store 0.21.1 from crates.io using debcargo 2.7.5 (Closes: #1090929)
  * Update relax-dep.patch for new upstream and current situation in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 27 Dec 2024 17:55:18 +0000

rust-cookie-store (0.21.0-1) unstable; urgency=medium

  * Package cookie_store 0.21.0 from crates.io using debcargo 2.6.11 (Closes: #1054041)
  * Update relax-dep.patch for new upstream and current situation in Debian.
  
  [ Reinhard Tartler ]
  * Team upload.
  * Package cookie_store 0.20.0 from crates.io using debcargo 2.6.1
  * Build against cookie 0.18

 -- Peter Michael Green <plugwash@debian.org>  Sun, 11 Feb 2024 21:51:01 +0000

rust-cookie-store (0.19.1-4) unstable; urgency=medium

  * Team upload.
  * Package cookie_store 0.19.1 from crates.io using debcargo 2.6.1
  * Relax dependencies on cookie and indexmap.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 08 Feb 2024 02:49:22 +0000

rust-cookie-store (0.19.1-3) unstable; urgency=medium

  * Team upload.
  * Package cookie_store 0.19.1 from crates.io using debcargo 2.6.0
  * Bump idna dependency to >= 0.3.0, < 0.5

 -- Peter Michael Green <plugwash@debian.org>  Sun, 27 Aug 2023 15:20:31 +0000

rust-cookie-store (0.19.1-2) unstable; urgency=medium

  * Team upload.
  * Package cookie_store 0.19.1 from crates.io using debcargo 2.6.0
  * Remove relax-dep.diff, it's no longer needed and accdiently specified
    a precise version of the time crate.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 01 Aug 2023 18:53:47 +0000

rust-cookie-store (0.19.1-1) unstable; urgency=medium

  * Team upload.
  * Package cookie_store 0.19.1 from crates.io using debcargo 2.6.0 (Closes: #1030822)
  * Drop old patches, no longer needed.
  * Relax dependency on time crate and disable wasm-bindgen feature
    since Debians version of time doesn't support it.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 28 Jul 2023 02:40:03 +0000

rust-cookie-store (0.16.1-2) unstable; urgency=medium

  * Team upload.
  * Package cookie_store 0.16.1 from crates.io using debcargo 2.5.0
  * Add patch for idna 0.3, thanks to Fabian Grünbichler.
  * Remove patch for publicsuffix 1.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 23 Oct 2022 11:00:23 +0000

rust-cookie-store (0.16.1-1) unstable; urgency=medium

  * Team upload.
  * Package cookie_store 0.16.1 from crates.io using debcargo 2.5.0
  * Add patch to continue using publicsuffix 1, publicsuffix 2 requires
    a new package.
  * Set collapse_features = true

  [ Ximin Luo ]
  * Package cookie_store 0.10.0 from crates.io using debcargo 2.4.0

 -- Peter Michael Green <plugwash@debian.org>  Sat, 18 Jun 2022 22:27:37 +0000

rust-cookie-store (0.8.0-1) unstable; urgency=medium

  * Package cookie_store 0.8.0 from crates.io using debcargo 2.2.10

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 13 Aug 2019 12:10:45 +0200
