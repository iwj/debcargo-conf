Source: rust-semver-0.9
Section: rust
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-sequence-cargo,
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-semver-parser-0.7+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Ximin Luo <infinity0@debian.org>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/semver-0.9]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/semver-0.9
Homepage: https://docs.rs/crate/semver/
X-Cargo-Crate: semver
Rules-Requires-Root: no

Package: librust-semver-0.9-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-semver-parser-0.7+default-dev
Suggests:
 librust-semver-0.9+serde-dev (= ${binary:Version})
Provides:
 librust-semver-0-dev (= ${binary:Version}),
 librust-semver-0+default-dev (= ${binary:Version}),
 librust-semver-0.9+default-dev (= ${binary:Version}),
 librust-semver-0.9.0-dev (= ${binary:Version}),
 librust-semver-0.9.0+default-dev (= ${binary:Version})
Replaces: librust-semver-dev (<< 0.9.1~)
Breaks: librust-semver-dev (<< 0.9.1~)
Description: Semantic version parsing and comparison - Rust source code
 Source code for Debianized Rust crate "semver"

Package: librust-semver-0.9+ci-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-semver-0.9-dev (= ${binary:Version}),
 librust-serde-1+default-dev
Provides:
 librust-semver-0+serde-dev (= ${binary:Version}),
 librust-semver-0.9+serde-dev (= ${binary:Version}),
 librust-semver-0.9+ci-dev (= ${binary:Version}),
 librust-semver-0.9.0+serde-dev (= ${binary:Version}),
 librust-semver-0.9.0+ci-dev (= ${binary:Version})
Description: Semantic version parsing and comparison - feature "serde" and 1 more
 This metapackage enables feature "serde" for the Rust semver crate, by pulling
 in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "ci" feature.
