From: Florent 'Skia' Jacquet <florent.jacquet@canonical.com>
Date: Wed, 10 Jul 2024 12:18:53 +0200
Subject: [PATCH] tests: bypass the proxy if testing timeouts

If an explicit proxy is configured in the environment, the timeout tests
will actually go through it, and then the test results will depend on
the proxy's performance, which can vary.

Testing this is very simple:
```
export http_proxy="http://127.0.0.1:1234"
cargo test timeout
```

We're hitting this issue in the Ubuntu CI. Further investigations
after 892569e10b69d1a0e5db1f53202d1ebf09924fc1 seemed to point that
**sometimes**, the proxy would be too slow (particularly on non amd64
architectures), leading to the tests to actually timeout and pass.

Bug-Ubuntu: https://bugs.launchpad.net/ubuntu/+source/rust-reqwest/+bug/2071789
Forwarded: https://github.com/seanmonstar/reqwest/pull/2349
---
 tests/timeouts.rs | 6 +++++-
 1 file changed, 5 insertions(+), 1 deletion(-)

diff --git a/tests/timeouts.rs b/tests/timeouts.rs
index 8b885c1..058b88f 100644
--- a/tests/timeouts.rs
+++ b/tests/timeouts.rs
@@ -18,6 +18,7 @@ async fn client_timeout() {
 
     let client = reqwest::Client::builder()
         .timeout(Duration::from_millis(500))
+        .no_proxy()
         .build()
         .unwrap();
 
@@ -43,7 +44,7 @@ async fn request_timeout() {
         }
     });
 
-    let client = reqwest::Client::builder().build().unwrap();
+    let client = reqwest::Client::builder().no_proxy().build().unwrap();
 
     let url = format!("http://{}/slow", server.addr());
 
@@ -70,6 +71,7 @@ async fn connect_timeout() {
 
     let client = reqwest::Client::builder()
         .connect_timeout(Duration::from_millis(100))
+        .no_proxy()
         .build()
         .unwrap();
 
@@ -101,6 +103,7 @@ async fn connect_many_timeout_succeeds() {
             &["10.255.255.1:81".parse().unwrap(), server.addr()],
         )
         .connect_timeout(Duration::from_millis(100))
+        .no_proxy()
         .build()
         .unwrap();
 
@@ -128,6 +131,7 @@ async fn connect_many_timeout() {
             ],
         )
         .connect_timeout(Duration::from_millis(100))
+        .no_proxy()
         .build()
         .unwrap();
 
