Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: finl_unicode
Upstream-Contact: https://github.com/dahosek/finl_unicode/issues
Source: https://github.com/dahosek/finl_unicode

Files: *
Copyright: Don Hosek
License: MIT or Apache-2.0
Comment: Unicode copyright attribution is unncessary
 The copyright attribution to unicode mentioned in README.md is not valid in
 debian as the mentioned unicode data (files) are not included in the source.
 Instead the unicode-data package is used to get the relevant files, d/rules
 contains the instructions for symliking them during build time.

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Ananthu C V <weepingclown@disroot.org>
License: MIT or Apache-2.0

Files:
 debian/missing-sources/categories.rs
 debian/missing-sources/generate-sources/Cargo.toml
 debian/missing-sources/generate-sources/src/main.rs
Copyright: Don Hosek
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
