Origin: vendor
Subject: fix some stuff in upstreams manpage

the 1.0.2 release has further fixes to manpage generation so that then hopefully
this patch can be dropped.

diff --git a/sq-keyring-linter.1 b/sq-keyring-linter.1
index 5f58242..ab60ed6 100644
--- a/sq-keyring-linter.1
+++ b/sq-keyring-linter.1
@@ -1,140 +1,104 @@
-.\" DO NOT MODIFY THIS FILE!  It was generated by help2man 1.48.1.
-.TH SEQUOIA-KEYRING-LINTER "1" "February 2023" "sequoia-keyring-linter 1.0.0" "User Commands"
+.ie \n(.g .ds Aq \(aq
+.el .ds Aq '
+.TH sequoia-keyring-linter 1  "sequoia-keyring-linter 1.0.1" 
 .SH NAME
-sequoia-keyring-linter \- sq-keyring-linter
+sequoia\-keyring\-linter \- A linter for keyrings
 .SH SYNOPSIS
-.B sq-keyring-linter
-[\fI\,OPTIONS\/\fR] [\fI\,FILE\/\fR]...
+\fBsequoia\-keyring\-linter\fR [\fB\-t\fR|\fB\-\-time\fR] [\fB\-q\fR|\fB\-\-quiet\fR] [\fB\-f\fR|\fB\-\-fix\fR] [\fB\-e\fR|\fB\-\-export\-secret\-keys\fR] [\fB\-p\fR|\fB\-\-password\fR] [\fB\-k\fR|\fB\-\-list\-keys\fR] [\fB\-h\fR|\fB\-\-help\fR] [\fB\-V\fR|\fB\-\-version\fR] [\fIFILE\fR] 
 .SH DESCRIPTION
 A linter for keyrings
-.SS "Arguments:"
-.IP
-[FILE]...
-.TP
-A list of OpenPGP keyrings to process.
-If none are specified,
-.IP
-a keyring is read from stdin.
 .SH OPTIONS
-.HP
-\fB\-t\fR, \fB\-\-time\fR <TIME>
-.IP
+.TP
+\fB\-t\fR, \fB\-\-time\fR=\fITIME\fR
 Sets the reference time to TIME.
+
+TIME is interpreted as an ISO 8601 timestamp. To set the reference time to July 21, 2013 at midnight UTC, you can do:
+
+$ sq\-keyring\-linter \-\-time 20130721 ...
+
+To include a time, add a T, the time and optionally the timezone (the default timezone is UTC):
+
+$ sq\-keyring\-linter \-\-time 20130721T0550+0200 ...
 .TP
-TIME is interpreted as an ISO 8601 timestamp.
-To set the reference time to July 21, 2013
-.IP
-at midnight UTC, you can do:
-.IP
-\f(CW$ sq-keyring-linter --time 20130721 ...\fR
-.IP
-To include a time, add a T, the time and optionally the timezone (the default timezone is
-UTC):
-.IP
-\f(CW$ sq-keyring-linter --time 20130721T0550+0200 ...\fR
-.HP
-\fB\-q\fR, \fB\-\-quiet\fR
-.IP
+\fB\-q\fR, \fB\-\-quiet\fR=\fIQUIET\fR
 Quiet; does not output any diagnostics
-.HP
-\fB\-f\fR, \fB\-\-fix\fR
-.IP
+.TP
+\fB\-f\fR, \fB\-\-fix\fR=\fIFIX\fR
 Attempts to fix certificates, when possible
-.HP
-\fB\-e\fR, \fB\-\-export\-secret\-keys\fR
-.IP
-When fixing a certificate, the fixed certificate is exported
-without any secret key material.  Using this switch causes any
-secret key material to also be exported.
-.HP
-\fB\-p\fR, \fB\-\-password\fR <PASSWORD>
 .TP
-A key's password.
-Normally this is not needed: if stdin is
-.IP
-connected to a tty, the linter will ask for a password when
-needed.
-.HP
-\fB\-k\fR, \fB\-\-list\-keys\fR
-.IP
-If set, outputs a list of fingerprints, one per line, of
-certificates that have issues.  This output is intended for
-use by scripts.
-.IP
-This option implies "\-\-quiet". If you also specify "\-\-fix",
-errors will still be printed to stderr, and fixed certificates
-will still be emitted to stdout.
-.HP
+\fB\-e\fR, \fB\-\-export\-secret\-keys\fR=\fIEXPORT_SECRET_KEYS\fR
+When fixing a certificate, the fixed certificate is exported without any secret key material. Using this switch causes any secret key material to also be exported.
+.TP
+\fB\-p\fR, \fB\-\-password\fR=\fIPASSWORD\fR
+A key\*(Aqs password. Normally this is not needed: if stdin is connected to a tty, the linter will ask for a password when needed.
+.TP
+\fB\-k\fR, \fB\-\-list\-keys\fR=\fILIST_KEYS\fR
+If set, outputs a list of fingerprints, one per line, of certificates that have issues. This output is intended for use by scripts.
+
+This option implies "\-\-quiet". If you also specify "\-\-fix", errors will still be printed to stderr, and fixed certificates will still be emitted to stdout.
+.TP
 \fB\-h\fR, \fB\-\-help\fR
-.IP
 Print help information (use `\-h` for a summary)
-.HP
+.TP
 \fB\-V\fR, \fB\-\-version\fR
-.IP
 Print version information
-.PP
+.TP
+[\fIFILE\fR]
+A list of OpenPGP keyrings to process. If none are specified, a keyring is read from stdin.
+.SH EXTRA
 `sq\-keyring\-linter` checks the supplied certificates for the following
 SHA\-1\-related issues:
-.IP
-\- Whether a certificate revocation uses SHA\-1.
-.IP
-\- Whether the current self signature for a non\-revoked User ID uses
-.IP
-SHA\-1.
-.IP
-\- Whether the current subkey binding signature for a non\-revoked,
-.IP
-live subkey uses SHA\-1.
-.IP
-\- Whether a primary key binding signature ("backsig") for a
-.IP
-non\-revoked, live subkey uses SHA\-1.
-.PP
+
+  \- Whether a certificate revocation uses SHA\-1.
+
+  \- Whether the current self signature for a non\-revoked User ID uses
+    SHA\-1.
+
+  \- Whether the current subkey binding signature for a non\-revoked,
+    live subkey uses SHA\-1.
+
+  \- Whether a primary key binding signature ("backsig") for a
+    non\-revoked, live subkey uses SHA\-1.
+
 Diagnostics are printed to stderr.  At the end, some statistics are
 shown.  This is useful when examining a keyring.  If `\-\-fix` is
 specified and at least one issue could be fixed, the fixed
 certificates are printed to stdout.
-.PP
+
 This tool does not currently support smart cards.  But, if only the
 subkeys are on a smart card, this tool may still be able to partially
 repair the certificate.  In particular, it will be able to fix any
 issues with User ID self signatures and subkey binding signatures for
 encryption\-capable subkeys, but it will not be able to generate new
 primary key binding signatures for any signing\-capable subkeys.
-.PP
+
 EXIT STATUS:
-.SS "If `--fix` is not specified:"
-.TP
-2
-if any issues were found,
-.TP
-1
-if not issues were found, but there were errors reading the input,
-.TP
-0
-if there were no issues.
-.SS "If `--fix` is specified:"
-.TP
-3
-if any issues could not be fixed,
-.TP
-1
-if not issues were found, but there were errors reading the input,
-.TP
-0
-if all issues were fixed or there were no issues.
-.PP
+
+If `\-\-fix` is not specified:
+  2  if any issues were found,
+  1  if not issues were found, but there were errors reading the input,
+  0  if there were no issues.
+
+If `\-\-fix` is specified:
+  3  if any issues could not be fixed,
+  1  if not issues were found, but there were errors reading the input,
+  0  if all issues were fixed or there were no issues.
+
 EXAMPLES:
-.IP
-# To gather statistics, simply run:
-$ sq\-keyring\-linter keyring.pgp
-.IP
-# To fix a key:
-$ gpg \fB\-\-export\-secret\-keys\fR FPR | sq\-keyring\-linter \fB\-\-fix\fR \fB\-p\fR passw0rd \fB\-p\fR password123 | gpg \fB\-\-import\fR
-.IP
-# To get a list of keys with issues:
-$ sq\-keyring\-linter \fB\-\-list\-keys\fR keyring.pgp | while read FPR; do something; done
-.PP
+
+  # To gather statistics, simply run:
+  $ sq\-keyring\-linter keyring.pgp
+
+  # To fix a key:
+  $ gpg \-\-export\-secret\-keys FPR | sq\-keyring\-linter \-\-fix \-p passw0rd \-p password123 | gpg \-\-import
+
+  # To get a list of keys with issues:
+  $ sq\-keyring\-linter \-\-list\-keys keyring.pgp | while read FPR; do something; done
+
 SEE ALSO:
-.PP
-sq\-keyring\-linter's homepage: <https://gitlab.com/sequoia\-pgp/keyring\-linter>
+
+sq\-keyring\-linter\*(Aqs homepage: <https://gitlab.com/sequoia\-pgp/keyring\-linter>
+.SH VERSION
+v1.0.1
+.SH AUTHORS
+Neal Walfield <neal@sequoia\-pgp.org>
diff --git a/src/cli.rs b/src/cli.rs
index 9bf3b10..cae50e6 100644
--- a/src/cli.rs
+++ b/src/cli.rs
@@ -2,8 +2,7 @@ use std::ffi::OsString;
 
 use clap::Parser;
 
-/// Checks for and optionally repairs OpenPGP certificates that use
-/// SHA-1.
+/// Checks for and optionally repairs OpenPGP certificates that use SHA-1.
 #[derive(Parser)]
 #[command(author, version, about, long_about = None, verbatim_doc_comment,
           after_help="\
@@ -65,14 +64,11 @@ pub struct Linter {
 
     /// Sets the reference time to TIME.
     ///
-    /// TIME is interpreted as an ISO 8601 timestamp.  To set the
-    /// reference time to July 21, 2013 at midnight UTC, you can
-    /// do:
+    /// TIME is interpreted as an ISO 8601 timestamp. To set the reference time to July 21, 2013 at midnight UTC, you can do:
     ///
     ///   $ sq-keyring-linter --time 20130721 ...
     ///
-    /// To include a time, add a T, the time and optionally the
-    /// timezone (the default timezone is UTC):
+    /// To include a time, add a T, the time and optionally the timezone (the default timezone is UTC):
     ///
     ///   $ sq-keyring-linter --time 20130721T0550+0200 ...
     #[arg(short, long)]
@@ -86,15 +82,11 @@ pub struct Linter {
     #[arg(short, long)]
     pub fix: bool,
 
-    /// When fixing a certificate, the fixed certificate is exported
-    /// without any secret key material.  Using this switch causes any
-    /// secret key material to also be exported.
+    /// When fixing a certificate, the fixed certificate is exported without any secret key material. Using this switch causes any secret key material to also be exported.
     #[arg(short, long, verbatim_doc_comment)]
     pub export_secret_keys: bool,
 
-    /// A key's password.  Normally this is not needed: if stdin is
-    /// connected to a tty, the linter will ask for a password when
-    /// needed.
+    /// A key's password. Normally this is not needed: if stdin is connected to a tty, the linter will ask for a password when needed.
     #[arg(
         short,
         long,
@@ -102,18 +94,13 @@ pub struct Linter {
     )]
     pub password: Vec<String>,
 
-    /// If set, outputs a list of fingerprints, one per line, of
-    /// certificates that have issues.  This output is intended for
-    /// use by scripts.
+    /// If set, outputs a list of fingerprints, one per line, of certificates that have issues. This output is intended for use by scripts.
     ///
-    /// This option implies "--quiet". If you also specify "--fix",
-    /// errors will still be printed to stderr, and fixed certificates
-    /// will still be emitted to stdout.
+    /// This option implies "--quiet". If you also specify "--fix", errors will still be printed to stderr, and fixed certificates will still be emitted to stdout.
     #[arg(short='k', long, verbatim_doc_comment)]
     pub list_keys: bool,
 
-    /// A list of OpenPGP keyrings to process.  If none are specified,
-    /// a keyring is read from stdin.
+    /// A list of OpenPGP keyrings to process. If none are specified, a keyring is read from stdin.
     #[arg(verbatim_doc_comment)]
     pub file: Vec<OsString>,
 }
